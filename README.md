# springboot-activity

#### 项目介绍
springboot集成activity

#### 软件架构
软件架构说明


#### 安装教程

#### 使用说明
1. 创建数据库activiti，修改applicaton中的数据库账号密码 
2. 启动 ActicityApplication，自动创建表
3. 访问 http://localhost:8080/model/create
![输入图片说明](https://images.gitee.com/uploads/images/2018/1130/170058_3f57be72_506962.png "QQ图片20181130170031.png")

4. 自定义流程并保存 http://localhost:8080/modeler.html?modelId=5001
5. 访问 http://localhost:8080/model/deploy/5001 部署流程
6. 待续

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)